
public class Activitat1 {

	public static void main(String[] args) {
		//Declaro las variables
		int num1 = 7, num2 = 12;
		//Muestro las variables con todas las operaciones
		System.out.println(num1 + " + " + num2 + " = " + (num1+num2));
		System.out.println(num1 + " - " + num2 + " = " + (num1-num2));
		System.out.println(num1 + " * " + num2 + " = " + (num1*num2));
		System.out.println(num1 + " / " + num2 + " = " + ((double)num1/num2));
		System.out.println(num1 + " % " + num2 + " = " + (num1%num2));
	}

}
